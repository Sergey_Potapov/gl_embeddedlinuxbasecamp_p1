#! /bin/bash
count=$(find ./ -type f -mtime +30 | wc -l)
array=()
array=(`find ./ -type f -mtime +30`)

i=0
while [ $i -lt $count  ]
do
name=$(basename ${array[$i]})
newName="~$name"
mv $name $newName
((i++))
done

if [ $count -gt 0 ]; then
	./../exercise01/ex1 -./
fi
