#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/time.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/list.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Sergey Potapov");
MODULE_DESCRIPTION("Example 10");
MODULE_VERSION("0.1");

#define BUFFER_SIZE     100
#define PROC_DIRECTORY  "example"
#define MODULE_TAG      "example_module "

static char buf_sys_msg[BUFFER_SIZE] = "0\n";

int size = 5;
module_param( size, int, S_IRUGO | S_IWUSR );

char test_str[30];

struct list_head *iter, *iter_safe;
struct data *item;
LIST_HEAD( list );

struct data {
    int n;
    char msg[30];
    char user[30];
    u32 publicTime;
    u32 delayTime;
    struct list_head list;
};


static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf)
{

	list_for_each( iter, &list ) {
	      item = list_entry( iter, struct data, list );
	      if((item->publicTime + item->delayTime) < (jiffies / HZ) ){
	      	sprintf(buf,"\n%s\n",item->msg);
	      	list_del( iter );
      		kfree( item );
	      	return strlen(buf);
	      }

   	}
	strcpy(buf, "\nno messages\n");
	return strlen(buf);
}

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	strncpy(buf_sys_msg, buf, count);
	buf_sys_msg[count] = '\0';
	
	item = kmalloc( sizeof(*item), GFP_KERNEL );
	sscanf(buf_sys_msg,"%i:%s",&(item->delayTime),&(item->msg));
	list_add( &(item->list), &list );
	
	item->publicTime = jiffies / HZ ;

	
	printk( KERN_INFO "\nIn add\n" );
   	list_for_each( iter, &list ) {
      		item = list_entry( iter, struct data, list );
      		printk( KERN_INFO "[LIST] pub:%d delay:%d %s\n", item->publicTime, item->delayTime, item->msg );
  	 }
	
	return count;
}

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)
CLASS_ATTR(xxx, (S_IWUSR | S_IRUGO), &x_show, &x_store);

static struct class *sys_class;

void test_lists( void ) {

   int i;
   
  /* for( i = 0; i < size; i++ ) {
      item = kmalloc( sizeof(*item), GFP_KERNEL );
     // if( !item ) goto out;
      item->n = i;
      
      sprintf(test_str,"%u:message%u",i,i);
      sscanf(test_str,"%i:%s",&(item->n),&(item->msg));
      //sprintf(item->msg,"%u:message%u",i);
      list_add( &(item->list), &list );
   }
   printk( KERN_INFO "\nIn init\n" );
   list_for_each( iter, &list ) {
      item = list_entry( iter, struct data, list );
      printk( KERN_INFO "[LIST] %d %s\n", item->n, item->msg );
   }
*/

}

static int __init mod_init( void ) {
   test_lists();
   
   int err;

   sys_class = class_create(THIS_MODULE, PROC_DIRECTORY);
   if (IS_ERR(sys_class))
	printk("bad class create\n");
   err = class_create_file(sys_class, &class_attr_xxx);
   if (err)
	goto error;

    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);

    return err;

}

static void __exit mod_exit(void)
{
   printk( KERN_INFO "\nIn exit\n" );
   list_for_each( iter, &list ) {
      item = list_entry( iter, struct data, list );
      printk( KERN_INFO "[LIST] %d %s\n", item->n, item->msg );
   }

   list_for_each_safe( iter, iter_safe, &list ) {
      item = list_entry( iter, struct data, list );
      list_del( iter );
      kfree( item );
   }

    class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);
}  
   
module_init( mod_init );
module_exit(mod_exit);
