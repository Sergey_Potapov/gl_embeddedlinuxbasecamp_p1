#!/bin/bash

DISPLAY_MODE="Full"
CURRENT_DIR="\."
SHOWDIR_FLG="true"
SHOWONLYDIR_FLG="false"

hideDirectories(){
	SHOWDIR_FLG="false"
}
hideFilesShowDirs(){
	SHOWDIR_FLG="true"
	SHOWONLYDIR_FLG="true"
}
unHideAll(){
	SHOWDIR_FLG="true"
	SHOWONLYDIR_FLG="false"
}
showDirectory(){
	echo -e "$(pwd):\n"
	CURRENT_DIR=$(pwd)
	if [[ $DISPLAY_MODE == "Full" ]]; then
			for entry in $(ls -a $CURRENT_DIR)
			do 
			if [[ -d "$entry" ]] 					&& [[ $SHOWDIR_FLG == "true" ]]; then
					echo "$entry"
				fi
			if [[ -f "$entry" ]] && [[ $SHOWONLYDIR_FLG == 				"false" ]]; then
					echo "$entry"
				fi
			done
		else 
			for entry in $(ls  $CURRENT_DIR)
			do 
				echo "$entry"
			done
	fi
}

showMenu(){
	echo -e "\n"
	echo "1.Change display mode (Current mode is 	$DISPLAY_MODE)"
	echo "2.Change directory"
	echo "3.Copy"
	echo "4.Move/Rename"
	echo "5.Delete"
	echo "6.Exit"
	echo "7.Don't show directories"
	echo "8.Show only directories"
	echo "9.Show all"	
}

changeMode(){
	if [[ $DISPLAY_MODE == "Full" ]]; then
		DISPLAY_MODE="Short"
	else
		DISPLAY_MODE="Full"
	fi
	echo Changing display mode. Current mode is 		$DISPLAY_MODE
}

changeDirectory(){
	echo "Enter the path:"
	read PATH1

	if [ ! -d "$PATH1" ]; then
		echo "Wrong directory"
	else
		cd $PATH1
	fi
}

copy(){
	echo "Enter the name of file or directory:"
	read SRC_NAME
	echo "Copy to:(enter the path)"
	read DST_PATH
	cp $SRC_NAME $DST_PATH
}


moveRename(){
	echo "Enter the filename:"
	read SRC_NAME
	echo "Do you want move or rename the file?"
	echo "1.Move"
	echo "2.Rename"
        read CHOICE
	case $CHOICE in
		1)
			echo "Enter the path:"
			read DST_NAME
			mv $SRC_NAME $DST_NAME			
			;;
		2)
			echo "Enter new name"
			read NEW_NAME
			mv $SRC_NAME $NEW_NAME			
			;;
	esac
}

delete(){
	echo "Enter the filename:"
	read FILENAME
	rm -r $FILENAME
}

#____MAIN____LOOP_________________

while [ 1 -le 2 ]
do

showDirectory
showMenu

read CHOICE

case $CHOICE in
	
	1)
		changeMode 
		;;
	2)
		changeDirectory
		;;
	3)
		copy
		;;
	4)
		moveRename
		;;
	5)
		delete
		;;	
	6)
		exit 0
		;;
	7)
		hideDirectories
		;;	
	8)
		hideFilesShowDirs
		;;
	9)      unHideAll
		;;
		
	*)
		echo "Unknown"
		;;
esac
done	
