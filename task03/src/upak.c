#include <stdio.h>
#include <stdint.h>

int main(void){
	char ch = ' ';
	int num = 0;
	int i = 0;
	while(1){
		ch = getchar();
		if(ch==EOF){
			break;		
		}
		if(i%2==0){
			num = ch;
		}else{
			for(uint32_t j = 0; j < num; j++){
				putchar(ch);			
			}
		}	
		i++;	
	}
}
