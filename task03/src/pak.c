#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>

struct list_node {
	struct list_node *next;
};

struct list_node *list_add(struct list_node *new, struct list_node *head){
	new->next = head;
	return new;
}

struct list_node *list_del_after(struct list_node *pos){
	struct list_node *deleted = pos->next;
	if(pos->next != NULL){
		pos->next = pos->next->next;
	}
	return deleted;
}

#define container_of(ptr, type, member) \
((type *)((char *)(ptr) - offsetof(type, member)))

#define list_for_each(pos) \
	for(;pos != NULL; pos = pos->next)

/*************************************************/

typedef struct s_codedChar{
	struct list_node list;
	char number;
	char code;
}t_codedChar;

t_codedChar *create_codedChar(int _number, char _code){
	t_codedChar  *result = malloc(sizeof(t_codedChar));
	result->number=_number;
	result->code=_code;
	result->list.next=NULL;
	return result;
}


t_codedChar currentCodedChar;
uint32_t counter;


int main(void){
	struct list_node *head;
	struct list_node *cursor;
	t_codedChar *element;

	uint32_t i = 0, counter = 0;

	char ch = getchar();
	currentCodedChar.code = ch;
	currentCodedChar.number = 1;
	ch = getchar();
	do{
		i++;
		if(currentCodedChar.code == ch){
			currentCodedChar.number++;		
		}else{
			element = create_codedChar(currentCodedChar.number, currentCodedChar.code);
			head = list_add(&element->list, head);
			currentCodedChar.code = ch;
			currentCodedChar.number = 1;
			counter++;
		}		
		ch = getchar();
	}while(ch!=EOF);
	
	char *out = malloc(counter*2);
	cursor = head;
	uint32_t cnt = counter-1;
	for(; cursor != NULL; cursor = cursor->next, cnt--){
		element = container_of(cursor, struct s_codedChar, list);
		out[cnt*2] = element->number;
		out[cnt*2+1] = element->code;
	}
	
	for(uint32_t i = 0; i!=counter*2; i+=2){
		putchar(out[i]);
		putchar(out[i+1]);
	}

	free(out);
	return 0;
}
