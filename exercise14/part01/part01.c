//#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>



 
clock_t mallocTest(size_t numBytes){
	char *p;
	clock_t startClock;
    	clock_t endClock;
    	clock_t _time;
	startClock = clock();
	
	p = malloc(numBytes);
	
	if(p!=NULL){
		p[0] = 1;
		p[numBytes - 1] = 1;
		endClock = clock();
		free(p);
		_time = endClock - startClock;
		return _time;
	}else{
		return 999;
	}	
}
 
clock_t callocTest(size_t numBytes){
	char *p;
	clock_t startClock;
    	clock_t endClock;
    	clock_t _time;
	startClock = clock();
	
	p = calloc(numBytes, sizeof(char));
	
	if(p!=NULL){
		p[0] = 1;
		p[numBytes - 1] = 1;
		endClock = clock();
		_time = endClock - startClock;
		free(p);
		return _time;
	}else{

		return 999;
	}	
}

clock_t allocaTest(size_t numBytes){
	char *p;
	clock_t startClock;
    	clock_t endClock;
    	clock_t _time;
	startClock = clock();
	
	p = alloca(numBytes);
	
	if(p!=NULL){
		p[0] = 1;
		p[numBytes - 1] = 1;
		endClock = clock();
		_time = endClock - startClock;

		return _time;
	}else{

		return 999;
	}	
}

void main() {

	size_t i = 1;
	printf("Block size \tmalloc(us) \tcalloc(us) \talloca(us)\n");

	for( i =1; i<5000000;i*=2)
	{
		printf("%10ld \t\t%ld \t\t%ld \t\t%ld\n",i,mallocTest(i),callocTest(i),allocaTest(i));
	}

}

