#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include<linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/kernel.h>
#include <linux/ktime.h>

MODULE_LICENSE("Dual BSD/GPL");

static u32 j;
char *p;
typedef struct stimeParam{
	ktime_t allocTime;
	ktime_t freeingTime;
}ttimeParam;

ttimeParam kmallocTime;
ttimeParam zmallocTime;
ttimeParam vmallocTime;

ktime_t kmallokTest(u32 numBytes)
{
	ktime_t startTime = 0;	
    	ktime_t endTime = 0;	
    	ktime_t _time = 0;
    	
    	startTime = ktime_get_real();

    	p = kmalloc(numBytes, GFP_KERNEL);
    	    	
    	if( p != NULL )
        {
            p[0]= 1;
            p[numBytes - 1]= 1;
           
            endTime = ktime_get_real();
        
            _time = endTime - startTime;
  
            kmallocTime.allocTime = _time;
            
            startTime = ktime_get_real();
            kfree(p);
            endTime = ktime_get_real();           
            _time = endTime - startTime;
            kmallocTime.freeingTime = _time;
            
            
            return _time;
           
            //printk( "size =%d kmalloc time =%lld", size_alloc[y], kmalloc_time );
        } else {
        	printk( "kmalloc allocation error\n");
        	return 999;
        }

}

ktime_t kzallokTest(u32 numBytes)
{
	ktime_t startTime = 0;	
    	ktime_t endTime = 0;	
    	ktime_t _time = 0;
    	
    	startTime = ktime_get_real();

    	p = kzalloc(numBytes, GFP_KERNEL);
    	    	
    	if( p != NULL )
        {
            p[0]= 1;
            p[numBytes - 1]= 1;
           
            endTime = ktime_get_real();
           
            _time = endTime - startTime;
            
            zmallocTime.allocTime = _time;
            startTime = ktime_get_real();
            kfree(p);
            endTime = ktime_get_real();           
            _time = endTime - startTime;
            zmallocTime.freeingTime = _time;
            
            
            return _time;
           
           

        } else {
        	printk( "kzalloc allocation error\n");
        	return 999;
        }

}

ktime_t vmallokTest(u32 numBytes)
{
	ktime_t startTime = 0;	
    	ktime_t endTime = 0;	
    	ktime_t _time = 0;
    	
    	startTime = ktime_get_real();

    	p = vmalloc(numBytes);
    	    	
    	if( p != NULL )
        {
            p[0]= 1;
            p[numBytes - 1]= 1;
           
            endTime = ktime_get_real();
           
            _time = endTime - startTime;
            
            vmallocTime.allocTime = _time;
            startTime = ktime_get_real();
            vfree(p);
            endTime = ktime_get_real();           
            _time = endTime - startTime;
            vmallocTime.freeingTime = _time;
     
           
            return _time;
           

        } else {
        	printk( "kzalloc allocation error\n");
        	return 999;
        }

}


static int __init init( void ) 
{
   u32 i = 1; 
      	printk( KERN_INFO "buffer size \t\tkmalloc(ns) \t\tkzalloc(ns) \tvmalloc(ns)\n" ); 
   for( i = 1; i < 600000; i*=2)
   {
   kmallokTest(i);
   kzallokTest(i);
   vmallokTest(i);
   	printk( KERN_INFO "%10i \t!%8lu %8lu !\t%8lu %8lu !\t%8lu %8lu\n", i, 
   	kmallocTime.allocTime, kmallocTime.freeingTime, 
   	zmallocTime.allocTime, zmallocTime.freeingTime, 
   	vmallocTime.allocTime, vmallocTime.freeingTime);   
   }
   return 0;
}

void cleanup( void ) {

   return;
}

module_init( init );
module_exit( cleanup );


