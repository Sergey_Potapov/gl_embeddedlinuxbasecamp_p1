#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section("__versions") = {
	{ 0xa11440c8, "module_layout" },
	{ 0x783cb62d, "param_ops_charp" },
	{ 0x5b6dfd24, "class_destroy" },
	{ 0x1111f175, "class_remove_file_ns" },
	{ 0x54a9396e, "i2c_del_driver" },
	{ 0x6f25b3e8, "class_create_file_ns" },
	{ 0x49d2824e, "__class_create" },
	{ 0xc5850110, "printk" },
	{ 0x638e63d8, "i2c_register_driver" },
	{ 0x86332725, "__stack_chk_fail" },
	{ 0xce92d5e3, "of_property_read_string" },
	{ 0x85fb927f, "of_property_read_variable_u32_array" },
	{ 0x8f678b07, "__stack_chk_guard" },
	{ 0x97255bdf, "strlen" },
	{ 0x3c3ff9fd, "sprintf" },
	{ 0xec09f6d2, "_dev_info" },
	{ 0x7045fa1f, "i2c_smbus_read_byte_data" },
	{ 0xb1ad28e0, "__gnu_mcount_nc" },
};

MODULE_INFO(depends, "");

MODULE_ALIAS("i2c:my_ds3231");

MODULE_INFO(srcversion, "7A5A999FFCB6E87F33BA8E8");
