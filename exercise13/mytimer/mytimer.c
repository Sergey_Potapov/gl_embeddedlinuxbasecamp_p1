/*******************************************************************************
* Copyright (c) 2015 Song Yang @ ittraining
* 
* All rights reserved.
* This program is free to use, but the ban on selling behavior.
* Modify the program must keep all the original text description.
*
* Email: onionys@ittraining.com.tw
* Blog : http://blog.ittraining.com.tw
*******************************************************************************/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/timer.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ITtraining.com.tw");
MODULE_DESCRIPTION("A timer example.");

struct timer_list my_timer;

/*
 * TIMER FUNCTION
 * */

static void timer_function(struct timer_list *tl){
	printk("Time up");
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(2000));
}

/*
 * INIT MODULE
 * */
int init(void)
{
	int ret;
	printk("Hello My Timer\n");

	//  -- initialize the timer
	timer_setup(&my_timer, timer_function, 0);
	
	//  -- setup the timer
	pr_info(" Setup timer to fire in 2s (%ld)\n", __func__, jiffies);
	
	ret = mod_timer(&my_timer, jiffies + msecs_to_jiffies(2000));
	if (ret)
		pr_err("%s: Timer firing failed\n", __func__);
	printk("END: init_module() \n");
	return 0;
}

/*
 * CLEANUP MODULE
 * */
void cleanup(void)
{
	del_timer(&my_timer);
	printk("Goodbye\n");
}

module_init( init );
module_exit( cleanup );
