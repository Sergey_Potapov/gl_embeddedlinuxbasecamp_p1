#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Aleksandr Bulyshchenko <A.Bulyshchenko@globallogic.com>");
MODULE_DESCRIPTION("Example for procfs read/write");
MODULE_VERSION("0.1");


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "example"
#define PROC_FILENAME   "UAH"
#define PROC_FILENAME_EUR   "EUR"
#define BUFFER_SIZE     10


static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;


static char *proc_buffer_eur;
static size_t proc_msg_length_eur;
static size_t proc_msg_read_pos_eur;

static struct proc_dir_entry *proc_dir_eur;
static struct proc_dir_entry *proc_file_eur;


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static ssize_t example_read_eur(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write_eur(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
//static struct file_operations proc_fops = {
static struct proc_ops proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};

static struct proc_ops proc_fops_eur = {
    .proc_read  = example_read_eur,
    .proc_write = example_write_eur,
};

static char buf_sys_msg[BUFFER_SIZE] = "1\n";

static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf)
{
	strcpy(buf, buf_sys_msg);
	return strlen(buf);
}

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	strncpy(buf_sys_msg, buf, count);
	buf_sys_msg[count] = '\0';
	return count;
}

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)
CLASS_ATTR(convFactor, (S_IWUSR | S_IRUGO), &x_show, &x_store);

static struct class *sys_class;

static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    proc_buffer_eur = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer_eur)
        return -ENOMEM;
    proc_msg_length_eur = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
    
    if (proc_buffer_eur) {
        kfree(proc_buffer_eur);
        proc_buffer_eur = NULL;
    }
    proc_msg_length_eur = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    proc_file_eur = proc_create(PROC_FILENAME_EUR, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops_eur);
    if (NULL == proc_file_eur)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_file_eur)
    {
        remove_proc_entry(PROC_FILENAME_EUR, proc_dir);
        proc_file_eur = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

char mybuf[10] = "test";


static ssize_t example_read_eur(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;

    if (length > (proc_msg_length_eur - proc_msg_read_pos_eur))
        length = (proc_msg_length_eur - proc_msg_read_pos_eur);

    
    left = copy_to_user(buffer, &proc_buffer_eur[proc_msg_read_pos_eur], length);

	//left = copy_to_user(mybuf, &proc_buffer[proc_msg_read_pos], length);
    
    proc_msg_read_pos_eur += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %lu from %lu chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %lu chars\n", length);

    return length - left;
}

static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    
    left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

	//left = copy_to_user(mybuf, &proc_buffer[proc_msg_read_pos], length);
    
    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %lu from %lu chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %lu chars\n", length);

    return length - left;
}


char uahbuf[10];
char eurbuf[10];
unsigned long int UAH = 0;
unsigned long int EUR = 0;
unsigned long int CURS = 0;
void sprint_fixed_pointer(unsigned long int val, char* str)
{
	unsigned long int val1 = val/100;
	unsigned long int val2 = val%100;
	sprintf(str, "%i.%02i" ,val1 ,val2);
};
void print_fixed_pointer(unsigned long int val)
{
	unsigned long int val1 = val/100;
	unsigned long int val2 = val%100;
	printk(KERN_INFO "%i.%02i" ,val1 ,val2);
};
unsigned long int convert(unsigned long int amount, unsigned long int curs)
{
	unsigned long int result = amount * curs;
	result/=100;
	return result;
}
void print_EUR_to_UAH(void){
	UAH = convert(EUR, CURS);
	sprint_fixed_pointer(EUR, eurbuf);
	sprint_fixed_pointer(UAH, uahbuf);
	printk(KERN_INFO "%s EUR = %s UAH" ,eurbuf ,uahbuf);
}


void sprint_UAH_to_EUR(unsigned long int _UAH){
	unsigned long int curs;
	sscanf(buf_sys_msg, "%lu", &curs);
	EUR = convert(_UAH*100, curs);
	sprint_fixed_pointer(EUR, eurbuf);
	sprint_fixed_pointer(UAH, uahbuf);
	printk(KERN_INFO "%s EUR = %s UAH" ,eurbuf ,uahbuf);
}
unsigned long int temp;
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %lu to %lu chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = copy_from_user(proc_buffer, buffer, msg_length);

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;
    
    
    sscanf(proc_buffer, "%lu" , &temp);
    
    sprint_UAH_to_EUR(temp);
    sprintf(proc_buffer_eur,"%s\n",eurbuf);
    
    proc_msg_length_eur = strlen(proc_buffer_eur);
    proc_msg_read_pos_eur = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %lu from %lu chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", msg_length);

    return length;
}



static ssize_t example_write_eur(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %lu to %lu chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;
//sscanf(%i,buffer);
    left = copy_from_user(proc_buffer, buffer, msg_length);

    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %lu from %lu chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %lu chars\n", msg_length);

    return length;
}


static int __init example_init(void)
{
    int err;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;


  sys_class = class_create(THIS_MODULE, PROC_DIRECTORY);
   if (IS_ERR(sys_class))
	printk("bad class create\n");
   err = class_create_file(sys_class, &class_attr_convFactor);
   if (err)
	goto error;


    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    class_remove_file(sys_class, &class_attr_convFactor);
    class_destroy(sys_class);

    return err;
}


static void __exit example_exit(void)
{
    cleanup_proc_example();
    cleanup_buffer();
    class_remove_file(sys_class, &class_attr_convFactor);
    class_destroy(sys_class);

    printk(KERN_NOTICE MODULE_TAG "exited\n");
}


module_init(example_init);
module_exit(example_exit);

