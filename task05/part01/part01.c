#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("xone");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.01");

int euro_cnt = 3;
int euro_cnt_ad = 10;
int hryvna_cnt;
int hryvna_cnt_ad;
int curs = 32;
int curs_ad = 0;
int temp;


unsigned long int UAH = 0;
unsigned long int EUR = 1931;
unsigned long int CURS = 3212;
char uahbuf[10];
char eurbuf[10];

void sprint_fixed_pointer(unsigned long int val, char* str)
{
	unsigned long int val1 = val/100;
	unsigned long int val2 = val%100;
	sprintf(str, "%i.%02i" ,val1 ,val2);
};
void print_fixed_pointer(unsigned long int val)
{
	unsigned long int val1 = val/100;
	unsigned long int val2 = val%100;
	printk(KERN_INFO "%i.%02i" ,val1 ,val2);
};
unsigned long int convert(unsigned long int amount, unsigned long int curs)
{
	unsigned long int result = amount * curs;
	result/=100;
	return result;
}
void print_EUR_to_UAH(void){
	UAH = convert(EUR, CURS);
	sprint_fixed_pointer(EUR, eurbuf);
	sprint_fixed_pointer(UAH, uahbuf);
	printk(KERN_INFO "%s EUR = %s UAH" ,eurbuf ,uahbuf);
}


static int __init ex01_init(void)
{
    print_EUR_to_UAH();
    return 0;
}

static void __exit ex01_exit(void)
{
    printk(KERN_INFO "End...\n");
}

module_init(ex01_init);
module_exit(ex01_exit);
