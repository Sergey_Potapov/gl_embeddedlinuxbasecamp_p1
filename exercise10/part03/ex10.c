#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Sergey Potapov");
MODULE_DESCRIPTION("Example 10");
MODULE_VERSION("0.1");


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "example"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     100


static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static char buf_sys_msg[BUFFER_SIZE] = "0\n";

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);



static struct proc_ops proc_fops = {
    .proc_read  = example_read,
    .proc_write = example_write,
};


static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}

static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf)
{
	strcpy(buf, buf_sys_msg);
	return strlen(buf);
}

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	strncpy(buf_sys_msg, buf, count);
	buf_sys_msg[count] = '\0';
	return count;
}

#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)
CLASS_ATTR(xxx, (S_IWUSR | S_IRUGO), &x_show, &x_store);

static struct class *sys_class;


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_proc_example(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}


static void cleanup_proc_example(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}
char tmp_buf[BUFFER_SIZE];

void reverseBuffer(char *buf, int length){
    int i = 0;
    for(i = 0; i < length; i++){
   	tmp_buf[i] = buf[length - i - 1];
    }
    for(i = 0; i < length; i++){
   	buf[i] = tmp_buf[i];
    }
}

void bufferToUpper(char *buf, int len){
	int i = 0;
	
	for(i = 0; i < len; i++){
		if((buf[i] > 96) && (buf[i] < 123)){
			buf[i] -= 32;
		}
	}
};


static ssize_t example_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
    size_t left;

    if (length > (proc_msg_length - proc_msg_read_pos))
        length = (proc_msg_length - proc_msg_read_pos);

    


    left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

    proc_msg_read_pos += length - left;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
    else
        printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

    return length - left;
}


static ssize_t example_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
    size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;

    left = copy_from_user(proc_buffer, buffer, msg_length);

    if (buf_sys_msg[0] == '1') {
    		reverseBuffer(proc_buffer, msg_length);
	}
    if (buf_sys_msg[0] == '2') {
		bufferToUpper(proc_buffer, msg_length);
	}


    proc_msg_length = msg_length - left;
    proc_msg_read_pos = 0;

    if (left)
        printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
    else
        printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

    return length;
}


static int __init example_init(void)
{
    int err;

    err = create_buffer();
    if (err)
        goto error;

    err = create_proc_example();
    if (err)
        goto error;

   sys_class = class_create(THIS_MODULE, PROC_DIRECTORY);
   if (IS_ERR(sys_class))
	printk("bad class create\n");
   err = class_create_file(sys_class, &class_attr_xxx);
   if (err)
	goto error;

    printk(KERN_NOTICE MODULE_TAG "loaded\n");
    return 0;

error:
    printk(KERN_ERR MODULE_TAG "failed to load\n");
    cleanup_proc_example();
    cleanup_buffer();
    class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);

    return err;
}


static void __exit example_exit(void)
{
    cleanup_proc_example();
    cleanup_buffer();
    class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);

    printk(KERN_NOTICE MODULE_TAG "exited\n");
}


module_init(example_init);
module_exit(example_exit);

