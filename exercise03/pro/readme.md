git config --global user.name "Sergey Potapov"
git config --global user.email "serhiypotapov8714@gmail.com"
git config --global core.autocrlf true
git config --global core.safecrlf true
mkdir exercise03
cd exercise03
git init
mkdir pro
cd pro
touch readme.md
git add .
git commit -m "repo: initial commit"
git checkout -b first_branch
gedit readme.md 
git status
git commit -am "readme: add the command log of the 1st subtask"
git checkout master
gedit readme.md
git commit -am "readme: add command log to solve 2nd subtask"
git log --oneline --decorate --graph --all
git status
git merge first_branch 
git mergetool
git status
git log --oneline --decorate --graph --all
gedit readme.md
git commit -am "readme: add command log to solve 3rd subtask"
gedit readme.md
git commit -am "readme: add command log to solve 4th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 5th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 6th subtask"
gedit readme.md
git commit -am "readme: add command log to solve 7th subtask"
git log --oneline --decorate --graph --all

