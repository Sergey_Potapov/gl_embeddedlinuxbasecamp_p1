#ifndef _MY_DS3231_H
#define _MY_DS3231_H

#define REG_SEC			0x00
#define REG_MIN			0x01
#define REG_HOUR		0x02
#define REG_TEMP_H		0x11
#define REG_TEMP_L		0x12
#define REG_DATE		0x04
#define REG_MONTH		0x05
#define REG_YEAR		0x06

#endif
