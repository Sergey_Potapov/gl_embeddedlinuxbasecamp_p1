#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "ili9341.h"
#include "fonts.c"
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/time.h>

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Sergey Potapov");
MODULE_DESCRIPTION("LCD Demo");

#define DATA_SIZE	90


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "example"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     100

static char buf_sys_msg[BUFFER_SIZE] = "test";

static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf);

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count);


#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)
CLASS_ATTR(xxx, (S_IWUSR | S_IRUGO), &x_show, &x_store);

static struct class *sys_class;

static u16 frame_buffer[LCD_WIDTH * LCD_HEIGHT];
static struct spi_device *lcd_spi_device;

static void lcd_reset(void)
{
	gpio_set_value(LCD_PIN_RESET, 0);
	mdelay(5);
	gpio_set_value(LCD_PIN_RESET, 1);
}

static void lcd_write_command(u8 cmd)
{
	gpio_set_value(LCD_PIN_DC, 0);
	spi_write(lcd_spi_device, &cmd, sizeof(cmd));
}

static void lcd_write_data(u8 *buff, size_t buff_size)
{
	size_t i = 0;
	
	gpio_set_value(LCD_PIN_DC, 1);
	while (buff_size > DATA_SIZE) {
		spi_write(lcd_spi_device, buff + i, DATA_SIZE);
		i += DATA_SIZE;
		buff_size -= DATA_SIZE;
	}
	spi_write(lcd_spi_device, buff + i, buff_size);
}

static void lcd_set_address_window(u16 x0, u16 y0, u16 x1, u16 y1)
{

	lcd_write_command(LCD_CASET);
	{
		uint8_t data[] = { (x0 >> 8) & 0xFF, x0 & 0xFF,
				 (x1 >> 8) & 0xFF, x1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RASET);
	{
		uint8_t data[] = { (y0 >> 8) & 0xFF, y0 & 0xFF,
				(y1 >> 8) & 0xFF, y1 & 0xFF };
		lcd_write_data(data, sizeof(data));
	}

	lcd_write_command(LCD_RAMWR);
}

inline void lcd_update_screen(void)
{
	lcd_write_data((u8*)frame_buffer, sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);
}

void lcd_draw_pixel(u16 x, u16 y, u16 color)
{
	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}
        
	frame_buffer[x + LCD_WIDTH * y] = (color >> 8) | (color << 8);
	lcd_update_screen();
}

void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color)
{
	u16 i;
	u16 j;

	if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
		return;
	}

	if ((x + w - 1) > LCD_WIDTH) {
		w = LCD_WIDTH - x;
	}

	if ((y + h - 1) > LCD_HEIGHT) {
		h = LCD_HEIGHT - y;
	}

	for (j = 0; j < h; j++) {
		for (i = 0; i < w; i++) {
			frame_buffer[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
		}
	}
	lcd_update_screen();
}

void lcd_fill_screen(u16 color)
{
	lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, color);
}

static void lcd_put_char(u16 x, u16 y, char ch, FontDef font, u16 color, u16 bgcolor) 
{
	u32 i, b, j;

	for (i = 0; i < font.height; i++) {
		b = font.data[(ch - 32) * font.height + i];
		for (j = 0; j < font.width; j++) {
			if ((b << j) & 0x8000)  {
				frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] = 
					(color >> 8) | (color << 8);
			} 
			else {
				frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] = 
					(bgcolor >> 8) | (bgcolor << 8);
			}
		}
	}
}

void lcd_put_str(u16 x, u16 y, const char* str, FontDef font, u16 color, u16 bgcolor) 
{
	while (*str) {
		if (x + font.width >= LCD_WIDTH) {
			x = 0;
			y += font.height;
			if (y + font.height >= LCD_HEIGHT) {
				break;
			}

			if (*str == ' ') {
				// skip spaces in the beginning of the new line
				str++;
				continue;
			}
		}
		lcd_put_char(x, y, *str, font, color, bgcolor);
		x += font.width;
		str++;
	}
}

void lcd_init_ili9341(void)
{
	// SOFTWARE RESET
	lcd_write_command(0x01);
	mdelay(1000);

	// POWER CONTROL A
	lcd_write_command(0xCB);
	{
		u8 data[] = { 0x39, 0x2C, 0x00, 0x34, 0x02 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL B
	lcd_write_command(0xCF);
	{
		u8 data[] = { 0x00, 0xC1, 0x30 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL A
	lcd_write_command(0xE8);
	{
		u8 data[] = { 0x85, 0x00, 0x78 };
		lcd_write_data(data, sizeof(data));
	}

	// DRIVER TIMING CONTROL B
	lcd_write_command(0xEA);
	{
		u8 data[] = { 0x00, 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER ON SEQUENCE CONTROL
	lcd_write_command(0xED);
	{
		u8 data[] = { 0x64, 0x03, 0x12, 0x81 };
		lcd_write_data(data, sizeof(data));
	}

	// PUMP RATIO CONTROL
	lcd_write_command(0xF7);
	{
		u8 data[] = { 0x20 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,VRH[5:0]
	lcd_write_command(0xC0);
	{
		u8 data[] = { 0x23 };
		lcd_write_data(data, sizeof(data));
	}

	// POWER CONTROL,SAP[2:0];BT[3:0]
	lcd_write_command(0xC1);
	{
		u8 data[] = { 0x10 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL
	lcd_write_command(0xC5);
	{
		u8 data[] = { 0x3E, 0x28 };
		lcd_write_data(data, sizeof(data));
	}

	// VCM CONTROL 2
	lcd_write_command(0xC7);
	{
		u8 data[] = { 0x86 };
		lcd_write_data(data, sizeof(data));
	}

	// PIXEL FORMAT
	lcd_write_command(0x3A);
	{
		u8 data[] = { 0x55 };
		lcd_write_data(data, sizeof(data));
	}
	
	// FRAME RATIO CONTROL, STANDARD RGB COLOR
	lcd_write_command(0xB1);
	{
		u8 data[] = { 0x00, 0x18 };
		lcd_write_data(data, sizeof(data));
	}
	
	// DISPLAY FUNCTION CONTROL
	lcd_write_command(0xB6);
	{
		u8 data[] = { 0x08, 0x82, 0x27 };
		lcd_write_data(data, sizeof(data));
	}

	// 3GAMMA FUNCTION DISABLE
	lcd_write_command(0xF2);
	{
		u8 data[] = { 0x00 };
		lcd_write_data(data, sizeof(data));
	}

	// GAMMA CURVE SELECTED
	lcd_write_command(0x26);
	{
		u8 data[] = { 0x01 };
		lcd_write_data(data, sizeof(data));
	}
	
	// POSITIVE GAMMA CORRECTION
	lcd_write_command(0xE0);
	{
		u8 data[] = { 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, 0x4E, 0xF1,
                           0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00 };
		lcd_write_data(data, sizeof(data));
	}
	
	// NEGATIVE GAMMA CORRECTION
	lcd_write_command(0xE1);
	{
		u8 data[] = { 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, 0x31, 0xC1,
                           0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F };
		lcd_write_data(data, sizeof(data));
	}

	// EXIT SLEEP
	lcd_write_command(0x11);
	mdelay(120);
    
	// TURN ON DISPLAY
	lcd_write_command(0x29);

	// MEMORY ACCESS CONTROL
	lcd_write_command(0x36);
	{
		u8 data[] = { 0x28 };
		lcd_write_data(data, sizeof(data));
	}

	// INVERSION
//	lcd_write_command(0x21);
}

static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf)
{
	
	strcpy(buf, buf_sys_msg);
	return strlen(buf);
}
char time_str[9] = "--:--:--";
char date_str[9] = "--------";
char temp_str[8] = "-- C";
char hum_str[9] = "-- %RH";
char temp2_str[8] = "-- C";

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	strncpy(buf_sys_msg, buf, count);
	buf_sys_msg[count] = '\0';
	
	switch(buf_sys_msg[0]){
		case '1':
			strncpy(time_str, buf_sys_msg+2, 8);
			break;
		case '2':
			strncpy(date_str, buf_sys_msg+2, 8);
			break;
		case '3':
			strncpy(temp_str, buf_sys_msg+2, 4);
			break;
		case '4':
			strncpy(hum_str, buf_sys_msg+2, 2);
			break;
		case '5':
			strncpy(temp2_str, buf_sys_msg+2, 4);
			break;
		}
	
	lcd_put_str(5, 20, time_str, Font_16x26, COLOR_WHITE, COLOR_BLUE);
	lcd_put_str(5, 50, date_str, Font_11x18, COLOR_WHITE, COLOR_BLUE);
	lcd_put_str(240, 20, temp_str, Font_11x18, COLOR_WHITE, COLOR_BLUE);
	lcd_put_str(240, 50, hum_str, Font_11x18, COLOR_WHITE, COLOR_BLUE);
	lcd_put_str(100, 150, temp2_str, Font_16x26, COLOR_WHITE, COLOR_BLUE);

	lcd_update_screen();
	return count;
}

static void __exit mod_exit(void)
{

	gpio_free(LCD_PIN_DC);
//	gpio_free(LCD_PIN_RESET);

	if (lcd_spi_device) {
		spi_unregister_device(lcd_spi_device);
	}
	pr_info("LCD: spi device unregistered\n");
	pr_info("LCD: module exited\n");
	class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);
}

static int __init mod_init(void)
{
	int ret;
	int err;
	struct spi_master *master;

	struct spi_board_info lcd_info = {
		.modalias = "LCD",
		.max_speed_hz = 25e6,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	};

	master = spi_busnum_to_master(lcd_info.bus_num);
	if (!master) {
		printk("MASTER not found.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device = spi_new_device(master, &lcd_info);
	if (!lcd_spi_device) {
		printk("FAILED to create slave.\n");
		ret = -ENODEV;
		goto out;
	}

	lcd_spi_device->bits_per_word = 8,

	ret = spi_setup(lcd_spi_device);
	if (ret) {
		printk("FAILED to setup slave.\n");
		spi_unregister_device(lcd_spi_device);
		ret = -ENODEV;
		goto out;
	}

	pr_info("LCD: spi device setup completed\n");

	gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
	gpio_direction_output(LCD_PIN_RESET, 0);
	gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
	gpio_direction_output(LCD_PIN_DC, 0);
	lcd_reset();

	lcd_init_ili9341();

	memset(frame_buffer, COLOR_GREEN, sizeof(frame_buffer));
	
	lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
	lcd_fill_rectangle(0, 0, LCD_WIDTH, LCD_HEIGHT, COLOR_BLUE);


	lcd_put_str(160, 20, "out.t:", Font_11x18, COLOR_WHITE, COLOR_BLUE);
	lcd_put_str(160, 50, "in.hum:", Font_11x18, COLOR_WHITE, COLOR_BLUE);
	lcd_put_str(40, 120, "indoor temperature:", Font_11x18, COLOR_WHITE, COLOR_BLUE);
	lcd_update_screen();
 	
 	pr_info("LCD: module loaded\n");

   sys_class = class_create(THIS_MODULE, PROC_DIRECTORY);
   if (IS_ERR(sys_class))
	printk("bad class create\n");
   err = class_create_file(sys_class, &class_attr_xxx);
   if (err)
	goto error;

	return 0;

out:
error:
    class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);
	return ret;
}

module_init(mod_init);
module_exit(mod_exit);


