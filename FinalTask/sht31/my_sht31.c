#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/delay.h>

static char *param1 = "C";
module_param(param1,charp,0660);

struct sht31_data {
	struct i2c_client *drv_client;
	uint32_t hum;
	uint8_t hum_frac;
	int32_t temp;
	uint8_t temp_frac;
};

static struct sht31_data sht_data;
char arr[6];

static int sht_read_data(void)
{
	uint32_t temp = 0;
	
	struct i2c_client *drv_client = sht_data.drv_client;

	if (drv_client == 0)
		return -ENODEV;
		
	i2c_smbus_write_byte_data(drv_client, 0x2C, 0x06);
	mdelay(20);
	i2c_smbus_read_i2c_block_data(drv_client, 0x00, 0x06, arr);

	temp = arr[0]*256 + arr[1];
	sht_data.temp_frac = ((1750 * temp)/65535)%10;
	temp = (175 * temp / 65535) - 45;
	sht_data.temp = temp;
	
	sht_data.hum = 100 * (arr[3] * 256 + arr[4]) / 65535;

	dev_info(&drv_client->dev, "read data\n");
	dev_info(&drv_client->dev, "hum: %d\n", sht_data.hum);
	dev_info(&drv_client->dev, "hum_frac: %d\n", sht_data.hum_frac);
	dev_info(&drv_client->dev, "temp: %d\n", sht_data.temp);
	dev_info(&drv_client->dev, "temp_frac: %d\n", sht_data.temp_frac);
	
	return 0;
}


static ssize_t sht_hum_show(struct class *class,
			    struct class_attribute *attr, char *buf)
{
	sht_read_data();
	
	sprintf(buf, "%02i ", sht_data.hum);
	return strlen(buf);
}


static ssize_t sht_temp_show(struct class *class,
			    struct class_attribute *attr, char *buf)
{
	sht_read_data();

	if (param1[0] == 'C') {
		sprintf(buf, "%i C", sht_data.temp);
	} 
	else if (param1[0] == 'F') {
		sprintf(buf, "%d %s", (sht_data.temp * 18 + 320)/10, param1);
	}
	else {
		sprintf(buf, "--");
	}

	return strlen(buf);
}


static int sht31_probe(struct i2c_client *drv_client, const struct i2c_device_id *id)
{

	int reg = 0;
	struct device_node *node;
	
	dev_info(&drv_client->dev,
		"i2c client address is 0x%X\n", drv_client->addr);
	dev_info(&drv_client->dev, "i2c driver probed\n");

	sht_data.drv_client = drv_client;
	sht_read_data();

	node =	drv_client->dev.of_node;
	of_property_read_u32(node, "reg", &reg);
	of_property_read_string(node, "param1", (const char**) &param1);
	dev_info(&drv_client->dev, "reg = %d\n", reg);
	dev_info(&drv_client->dev, "param1 = %s\n", param1);

	return 0;
}


static int sht31_remove(struct i2c_client *drv_client)
{

	sht_data.drv_client = 0;
	dev_info(&drv_client->dev, "i2c driver removed\n");
	
	return 0;
}


static const struct i2c_device_id sht31_idtable [] = {
    { "my_sht31", 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, sht31_idtable);

static struct i2c_driver sht31_i2c_driver = {
    .driver = {
   	 .name = "my_sht31",
    },

    .probe = sht31_probe,
    .remove = sht31_remove,
    .id_table = sht31_idtable,
};

CLASS_ATTR_RO(sht_hum);
CLASS_ATTR_RO(sht_temp);

static struct class *attr_class;


static int sht31_init(void)
{
	int ret;

	ret = i2c_add_driver(&sht31_i2c_driver);
	if (ret) {
		printk(KERN_ERR "sht31: failed to add new i2c driver: %d\n", ret);
		return ret;
	}
	printk(KERN_INFO "sht31: i2c driver created\n");

	printk(KERN_INFO "sht31: PARAM = %s\n", param1);


	attr_class = class_create(THIS_MODULE, "sht31");
	if (IS_ERR(attr_class)) {
		ret = PTR_ERR(attr_class);
		printk(KERN_ERR "sht31: failed to create sysfs class: %d\n", ret);
		return ret;
	}
	printk(KERN_INFO "sht31: sysfs class created\n");

	ret = class_create_file(attr_class, &class_attr_sht_hum);
	if (ret) {
		printk(KERN_ERR "sht31: failed to create sysfs class attribute sht_hum: %d\n", ret);
		return ret;
	}

	ret = class_create_file(attr_class, &class_attr_sht_temp);
	if (ret) {
		printk(KERN_ERR "sht31: failed to create sysfs class attribute sht_temp: %d\n", ret);
		return ret;
	}
		
	printk(KERN_INFO "sht31: sysfs class attributes created\n");
	printk(KERN_INFO "sht31: module loaded\n");

	return 0;
}


static void sht31_exit(void)
{

	if (attr_class) {
		class_remove_file(attr_class, &class_attr_sht_hum);
		class_remove_file(attr_class, &class_attr_sht_temp);
		printk(KERN_INFO "sht31: sysfs class attributes removed\n");

		class_destroy(attr_class);
		printk(KERN_INFO "sht31: sysfs class destroyed\n");
	}

	i2c_del_driver(&sht31_i2c_driver);
	printk(KERN_INFO "sht31: i2c driver deleted\n");
	printk(KERN_INFO "sht31: module exited\n");
}


module_init(sht31_init);
module_exit(sht31_exit);

MODULE_AUTHOR("Sergey Potapov");
MODULE_DESCRIPTION("sht31 Demo");
MODULE_LICENSE("GPL");
