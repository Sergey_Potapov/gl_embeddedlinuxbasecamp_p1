#include <linux/module.h>
#include <linux/jiffies.h>
#include <linux/types.h>

MODULE_LICENSE("Dual BSD/GPL");

static u32 j;
extern u32 lastRead;

static int __init init( void ) {
   j = jiffies; 
   printk( KERN_INFO "module: jiffies on start = %X\n", j );   
   return 0;
}

void cleanup( void ) {
   static u32 j1;
   j1 = jiffies; 
   //printk( KERN_INFO "module: jiffies on finish = %X\n", j1 );   
   printk( KERN_INFO "module: lastRead = %X\n", lastRead);  
   j = j1 - j;
   printk( KERN_INFO "module: interval of life = %d\n", j / HZ );   
   return;
}

module_init( init );
module_exit( cleanup );
