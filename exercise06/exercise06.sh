#! /bin/bash

COUNTER=0
BTN_STATE="0"
BTN_READ="0"

if ! [ -d /sys/class/gpio/gpio26 ]; then
	echo 26 > /sys/class/gpio/export
fi
sleep 0.1
sudo echo in > /sys/class/gpio/gpio26/direction
echo "Press the button"
while [[ 0 -ne 1 ]]; do
        sleep 0.1
  	BTN_READ=$(cat /sys/class/gpio/gpio26/value) 
	if [[ ! $BTN_STATE -eq $BTN_READ ]]; then 
    		BTN_STATE=$BTN_READ
		if [[ $BTN_READ -eq "1" ]]; then
			((COUNTER++))
			echo $COUNTER
		fi
	fi
done
