#include <stdio.h>
#include <time.h>

enum{LOSE, DRAW, WIN};

typedef struct s_gameStat{
	char* playerName;
	int result;
	int playerScore;
	int oppScore;
	long int dtime;
}t_gamestat;

char name[10];
int choice;
long int ttime;
char timeStr[30];
int gamesCounter;
t_gamestat currentGame;
t_gamestat *archive;

void addGameToArchive(t_gamestat game){
	t_gamestat *temp_archive;
	temp_archive = malloc(sizeof(t_gamestat) * gamesCounter);	
	if(gamesCounter>1){
		for(int i = 0; i < gamesCounter - 1; i++){
			memcpy(&temp_archive[i],&archive[i],sizeof(t_gamestat));		
		}	
	}
	memcpy(&temp_archive[gamesCounter - 1],&game,sizeof(t_gamestat));
	free(archive);
	archive = temp_archive;
}

void showArchive(){
	for(int i = 0; i < gamesCounter; i++)
	{
		printArchiveEntry(archive[i]);	
	}
}

void printArchiveEntry(t_gamestat game){
	printf("%s ", game.playerName);
	switch(game.result){
		case 0:printf("LOSE ");
			break;
		case 1:printf("DRAW ");
			break;
		case 2:printf("WIN ");
			break;	
	}
	printf("%d-%d", game.playerScore, game.oppScore);
	printf (" %s\n",ctime(&game.dtime) );
}

void game(t_gamestat *game){
	game->playerName = name;
	game->dtime = time (NULL);
	int yourScore = rand() % 6 + 1;
	game->playerScore = yourScore;
	int opponentScore = rand() % 6 + 1;
	game->oppScore = opponentScore;
	printf("Your score:%d\n", yourScore);
	printf("Opponent score:%d\n", opponentScore);
	if(yourScore > opponentScore){
		printf("\nYou win!\n");
		game->result = WIN;
	}else{
		if(yourScore == opponentScore){
			printf("\nDraw!\n");
			game->result = DRAW;
		}else{
			printf("\nYou lose!\n");
			game->result = LOSE;			
		}
	}
	gamesCounter++;
	addGameToArchive(*game);
}

void printMenu(void){
	printf("\n1. Start the game\n");
	printf("2. View game archive\n");
	printf("3. Exit\n");
}

int main(void){
	srand(time(NULL));
	printf("Enter your name:");
	scanf("%s", name);
	printf("\nWelcome %s!\n", name);

	while(choice != 3){
		printMenu();
		scanf("%d",&choice);
		switch(choice){
			case 1:
				game(&currentGame);

				break;
			case 2: showArchive();
				break;
			case 3:
				break;
			default:
				printf("Wrong number\n");
				break;
		}
	}
}
