#! /bin/bash

EXIT_COUNTER=0

gpioInit(){
if ! [ -d /sys/class/gpio/gpio26 ]; then
        echo 26 > /sys/class/gpio/export
fi
sleep 0.1
sudo echo in > /sys/class/gpio/gpio26/direction
}

showData(){
i2cset -y 1 0x68 0x0E 0x20
SECONDS_REG=$(i2cget -y 1 0x68 0x00)
MINUTES_REG=$(i2cget -y 1 0x68 0x01)
HOURS_REG=$(i2cget -y 1 0x68 0x02)
TEMPERATURE=$(i2cget -y 1 0x68 0x11)
TEMPERATURE_FRAC=$(i2cget -y 1 0x68 0x12)
let TEMPERATURE_FRAC=$TEMPERATURE_FRAC/64*25
printf " %02x:%02x:%02x t=%i.%i\n" $HOURS_REG $MINUTES_REG $SECONDS_REG $TEMPERATURE $TEMPERATURE_FRAC
}

btnApp(){
        BTN_READ=$(cat /sys/class/gpio/gpio26/value) 
        if [[ ! $BTN_STATE -eq $BTN_READ ]]; then 
                BTN_STATE=$BTN_READ
                if [[ $BTN_READ -eq "1" ]]; then
                        showData
			let EXIT_COUNTER=$EXIT_COUNTER+1 
	        fi
        fi

	if [[ $BTN_READ -eq "1" ]]; then
                let EXIT_COUNTER=$EXIT_COUNTER+1
	else
	EXIT_COUNTER=0; 
        fi

	if [ $EXIT_COUNTER -ge 10 ]; then
	exit
	fi
}

gpioInit
while [ 1 -eq 1 ]
do
sleep 0.1
btnApp
done
