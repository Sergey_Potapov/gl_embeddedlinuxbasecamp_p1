sudo apt install build-essential
sudo apt-get install git
git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable
cd linux-stable

mkdir build
export BUILD_KERNEL=~/linux-stable/build

sudo apt-get install flex
sudo apt-get install bison
make O=${BUILD_KERNEL} i386_defconfig

sudo apt-get install libncurses-dev
cd ${BUILD_KERNEL}


sudo apt-get install libssl-dev
make -j4


git clone git://git.buildroot.net/buildroot
cd buildroot
export BUILD_ROOTFS=~/linux-stable/build
make O=${BUILD_ROOTFS} qemu_x86_defconfig
cd ${BUILD_ROOTFS}

make menuconfig

● Target options:
○ Target Architecture = i386
○ Target Architecture Variant = i686

● Toolchain:
○ Custom kernel headers series = 4.13.x (should match the kernel)
○ [*] Enable WCHAR support
● System configuration:
○ System hostname = myLinux (give it a name)
○ System banner = Welcome to myLinux
○ [*] Enable root login with password
○ Root password = <rootpass>
○ Path to the users tables = ${BUILD_ROOTFS}/users (we'll create regular user)
○ Root filesystem overlay directories = ${BUILD_ROOTFS}/root (we'll put there some
additional configs)
● Kernel:
○ [ ] Linux Kernel (we'll use manually built kernel, so disable it here)

● Target packages:
○ [*] Show packages that are also provided by
busybox
○ Development tools
■ [*] binutils
■ [*] binutils binaries
■ [*] findutils
■ [*] grep
■ [*] sed
■ [*] tree
○ Libraries:
■ Compression and decompression:
■ [*] zlib
■ Text and terminal handling:
■ [*] ncurses
■ [*] readline
○ Networking applications:
■ [*] dropbear (ssh server)
■ [*] wget
○ Shell and utilities:
■ [*] bash
■ [*] file
■ [*] sudo
■ [*] which
○ System tools
■ [*] kmod
■ [*] kmod utilities
■ [*] rsyslog
○ Text editors and viewers:
■ [*] joe (it might be the easiest
terminal editor unless you're
familiar with vi)
■ [*] less
■ [*] mc
■ [*] vim
● Filesystem images:
○ [*] ext2/3/4 root filesystem
○ ext2/3/4 variant = ext3
○ [*] tar the root filesystem

Create user record
echo "user 1000 user 1000=pass /home/user/bin/bash - Linux User" > ${BUILD_ROOTFS}/users

○ Add the user to sudoers
mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d
echo "user ALL=(ALL) ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user

○ Create list of shells for dropbear
mkdir -p ${BUILD_ROOTFS}/root/etc
echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells
echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells

Install QEMU
sudo apt install qemu qemu-kvm libvirt-daemon libvirt-clients bridge-utils virt-manager

launch qemu:
qemu-system-i386 -kernel ${BUILD_KERNEL}/arch/i386/boot/bzImage -append "root=/dev/sda console=ttyS0" -drive format=raw,file=${BUILD_ROOTFS}/images/rootfs.ext3 -nic user,hostfwd=tcp::8022-:22 &
