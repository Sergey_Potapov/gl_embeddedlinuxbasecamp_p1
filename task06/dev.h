#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <asm/uaccess.h>
#define BUFFER_SIZE 1024
MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Oleg Tsiliuric <olej@front.ru>" );
MODULE_VERSION( "6.3" );
int offset;
//static char *hello_str = "Hello, world!\n";         // buffer!
static char hello_str[BUFFER_SIZE] = "Hello";         // buffer!

static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos ) {
   //int len = strlen( hello_str);
  int len = offset;
  // len = offset;
  //int len =100;
   printk( KERN_INFO "=== read : %ld\n", (long)count );
   if( count < len ) return -EINVAL;
   if( *ppos != 0 ) {
      printk( KERN_INFO "=== read return : 0\n" );  // EOF
      return 0;
   }
   if( copy_to_user( buf, hello_str, len ) ) return -EINVAL;
   *ppos = len;
   printk( KERN_INFO "=== read return : %d\n", len );
   return len;
}

//static ssize_t dev_write( struct file * file, char * buffer, size_t length, loff_t *ppos ) {
 static ssize_t dev_write( struct file * file, const char * buffer, size_t length, loff_t *ppos ) {                       
        size_t msg_length;
    size_t left;

    if (length > BUFFER_SIZE)
    {
        
        msg_length = BUFFER_SIZE;
    }
    else
        msg_length = length;
sprintf(hello_str+offset,"1:");
    left = copy_from_user(hello_str+2+offset, buffer, msg_length);
offset+=msg_length+2;
    //proc_msg_length = msg_length - left;
    //proc_msg_read_pos = 0;




   return length;
}

static int __init dev_init( void );
module_init( dev_init );

static void __exit dev_exit( void );
module_exit( dev_exit );

