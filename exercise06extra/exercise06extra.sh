#! /bin/bash

trap 'allLedOff; exit' SIGINT

BTN_STATE="0"
BTN_READ="0"

COUNTER=0
APP_STATE=0 #0 - idle, 1 - RGB, 2 - BGR
LED_STATE=0 #0 - all off 1-R 2-G 3-B

gpioInit(){
if ! [ -d /sys/class/gpio/gpio26 ]; then
        echo 26 > /sys/class/gpio/export
fi
if ! [ -d /sys/class/gpio/gpio16 ]; then
        echo 16 > /sys/class/gpio/export
fi
if ! [ -d /sys/class/gpio/gpio20 ]; then
        echo 20 > /sys/class/gpio/export
fi
if ! [ -d /sys/class/gpio/gpio21 ]; then
        echo 21 > /sys/class/gpio/export
fi
sleep 0.1
sudo echo in > /sys/class/gpio/gpio26/direction
sudo echo out > /sys/class/gpio/gpio16/direction
sudo echo out > /sys/class/gpio/gpio20/direction
sudo echo out > /sys/class/gpio/gpio21/direction
}

allLedOff(){
  echo 0 > /sys/class/gpio/gpio16/value
  echo 0 > /sys/class/gpio/gpio21/value
  echo 0 > /sys/class/gpio/gpio20/value   
}

redLedOn(){
  allLedOff
  echo 1 > /sys/class/gpio/gpio16/value 
}

greenLedOn(){
  allLedOff
  echo 1 > /sys/class/gpio/gpio20/value 
}

blueLedOn(){
  allLedOff
  echo 1 > /sys/class/gpio/gpio21/value 
}

btnApp(){
        BTN_READ=$(cat /sys/class/gpio/gpio26/value) 
        if [[ ! $BTN_STATE -eq $BTN_READ ]]; then 
                BTN_STATE=$BTN_READ
                if [[ $BTN_READ -eq "1" ]]; then
                        ((COUNTER++))
                        echo $COUNTER
                fi
        fi
}

changeAppState(){

case "$APP_STATE" in

	0)
	APP_STATE=1
	;;

	1)
	APP_STATE=2
	;;

	2)
	APP_STATE=1
	;;
esac
}

btnApp(){
        BTN_READ=$(cat /sys/class/gpio/gpio26/value) 
        if [[ ! $BTN_STATE -eq $BTN_READ ]]; then 
                BTN_STATE=$BTN_READ
                if [[ $BTN_READ -eq "1" ]]; then
                        changeAppState
                fi
        fi
}

lightApp(){
	case $LED_STATE in
		0) allLedOff
		   if [ $APP_STATE -eq 1 ];then
                        LED_STATE=1
			 fi
		   ;;
		1) if [ $APP_STATE -eq 1 ];then
			LED_STATE=2
		   else
			LED_STATE=3
		  fi ;;
                2) if [ $APP_STATE -eq 1 ];then
                        LED_STATE=3
                   else
                        LED_STATE=1
                  fi ;;
	        3) if [ $APP_STATE -eq 1 ];then
                        LED_STATE=1
                   else
                        LED_STATE=2
                  fi ;;
	esac	
}

ledOut(){

case $LED_STATE in
	0) allLedOff
	;;
	1) redLedOn
	;;
	2) greenLedOn
	;;
	3) blueLedOn
	;;
esac
}

gpioInit
while [ 1 -ne 2 ]
do

btnApp

if [[ COUNTER -eq 5 ]]; then
COUNTER=0
lightApp
fi

ledOut

#echo "app state:$APP_STATE led state:$LED_STATE"
sleep 0.1
((COUNTER++))
done


trap 'echo dont hang up' SIGINT
trap 'echo trap SIGINT' SIGINT
