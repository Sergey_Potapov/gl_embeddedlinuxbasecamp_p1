#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/jiffies.h>
#include <linux/types.h>
#include <linux/time.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/sched/task.h>
#include <asm/processor.h>
#include <linux/threads.h>  

///#include <kernel/fork.c>

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("LCD Demo");
int param = 3;
module_param( param, int, 0 );
#define DATA_SIZE	90


#define MODULE_TAG      "example_module "
#define PROC_DIRECTORY  "example"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     100

static char buf_sys_msg[BUFFER_SIZE] = "test";

static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf);

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count);


#define CLASS_ATTR(_name, _mode, _show, _store)                                \
	struct class_attribute class_attr_##_name =                            \
		__ATTR(_name, _mode, _show, _store)
CLASS_ATTR(xxx, (S_IWUSR | S_IRUGO), &x_show, &x_store);

static struct class *sys_class;



static ssize_t x_show(struct class *class, struct class_attribute *attr,
			char *buf)
{
	
	strcpy(buf, buf_sys_msg);
	return strlen(buf);
}
static int thread( void * data ) { 
   printk( KERN_INFO "thread: child process [%d] is running\n", current->pid ); 

   return 0; 
} 
/*
pid_t kernel_thread(int (*fn)(void *), void *arg, unsigned long flags)
{
	struct kernel_clone_args args = {
		.flags		= ((lower_32_bits(flags) | CLONE_VM |
				    CLONE_UNTRACED) & ~CSIGNAL),
		.exit_signal	= (lower_32_bits(flags) & CSIGNAL),
		.stack		= (unsigned long)fn,
		.stack_size	= (unsigned long)arg,
	};

	return kernel_clone(&args);
}*/

static ssize_t x_store(struct class *class, struct class_attribute *attr,
			 const char *buf, size_t count)
{
	
   pid_t pid; 
   printk( KERN_INFO "thread: main process [%d] is running\n", current->pid ); 
   pid = kernel_thread( thread, NULL, CLONE_FS );     /* Запускаем новый поток */ 
//pid = kernel_thread( thread, NULL, name );     /* Запускаем новый поток */ 


	return count;
}

static void __exit mod_exit(void)
{


	pr_info("LCD: spi device unregistered\n");
	pr_info("LCD: module exited\n");
	class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);
}







static int __init mod_init(void)
{


	int ret;
	int err;
	

   sys_class = class_create(THIS_MODULE, PROC_DIRECTORY);
   if (IS_ERR(sys_class))
	printk("bad class create\n");
   err = class_create_file(sys_class, &class_attr_xxx);
   if (err)
	goto error;

	return 0;

out:
error:
    class_remove_file(sys_class, &class_attr_xxx);
    class_destroy(sys_class);
	return ret;
}

module_init(mod_init);
module_exit(mod_exit);



